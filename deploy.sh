#!/bin/bash

source ~/.bashrc
cd ~/production/pos/pos-tgpp/pos-tgpp-apis
echo $DEPLOY_BRANCH
git checkout $DEPLOY_BRANCH
git status
git fetch && git reset --hard origin/$DEPLOY_BRANCH
yarn
yarn build
pm2 restart ecosystem.config.js
exit
exit