#!/bin/bash

if [ $# -ne 1 ] ; then
  echo "Usage: ssh-add-pass passfile"
  exit 1
fi

expect << EOF
  spawn ssh-add /opt/atlassian/pipelines/agent/ssh/id_rsa
  expect "Enter passphrase"
  send "$1\r"
  expect eof
EOF
