
echo $DEPLOY_BRANCH
git checkout $DEPLOY_BRANCH
git fetch && git reset --hard origin/$DEPLOY_BRANCH
yarn
yarn build
pm2 restart ecosystem.config.js