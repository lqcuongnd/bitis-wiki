module.exports = {
  apps : [{
    name: 'bitis-wiki',
    script: 'server/index.js',

    // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
    args: 'one two',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'staging'
    }
  }
};
